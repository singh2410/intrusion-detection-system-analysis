# Intrusion Detection System Analysis using Neural Networks
## By- Aarush Kumar
### Dated: July 15,2021

## Intrusion Detection System:
An Intrusion Detection System (IDS) is a system that monitors network traffic for suspicious activity and issues alerts when such activity is discovered. It is a software application that scans a network or a system for harmful activity or policy breaching. Any malicious venture or violation is normally reported either to an administrator or collected centrally using a security information and event management (SIEM) system. A SIEM system integrates outputs from multiple sources and uses alarm filtering techniques to differentiate malicious activity from false alarms.
### Steps involved in Intrusion Detection System Project are as follows:
### 1- Loading of Data:
![Screenshot_from_2021-07-15_17-11-52](/uploads/d085583dea90e7f318dc3814384b7d17/Screenshot_from_2021-07-15_17-11-52.png)
### 2- Exploratory Data Analysis (EDA):
![Screenshot_from_2021-07-15_17-12-35](/uploads/0c093ec9fdf06fffceb2ca59fbbaeb40/Screenshot_from_2021-07-15_17-12-35.png)
### 3- Visualization:
![Screenshot_from_2021-07-15_17-12-59](/uploads/652e2222e84471b12a493e071ef5f6c4/Screenshot_from_2021-07-15_17-12-59.png)
![Screenshot_from_2021-07-15_17-13-22](/uploads/91aaf1cb1a009d3a31dbd15fc8c0f46e/Screenshot_from_2021-07-15_17-13-22.png)
### 4- Building Model:
![Screenshot_from_2021-07-15_17-13-53](/uploads/bc8479bbd256cc41851965786e8be15f/Screenshot_from_2021-07-15_17-13-53.png)
### 5- Testing Model Accuracy & Confusion Matrix:
![Screenshot_from_2021-07-15_17-14-15](/uploads/d10f93f67eb56ad11e900f832d516c89/Screenshot_from_2021-07-15_17-14-15.png)
### 6- Conclusion:
We achieved really impressive results with our model predictions on the validation and testing sets! We got F1 Scores of 95.873% and 95.330% respectively! For all types of attacks, except for nmap and other attacks, we got >95% accuracies. For other attacks, this is not suprising; That label is a conglomerate of various attacks, some with significantly few observations to adequately train on. If we wish to acheive better results with this label, or even predicting the specific attack, we just need to capture and add more such observations to our training set. For the nmap attack, it is probably due to the fact that it has similar enough metrics to other attacks in the dataset, specifically ipsweep. This is understandable as both nmap and ipsweep activities, aim to figure out how many live IP's there are.
## Thankyou!



